package com.calculator.ascentio.components;

public interface Operation {
	public Double calculate(Double a, Double b);
	public Integer precedence();
}
