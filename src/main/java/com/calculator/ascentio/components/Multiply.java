package com.calculator.ascentio.components;

import org.springframework.stereotype.Component;

@Component
public class Multiply implements Operation {

	@Override
	public Double calculate(Double a, Double b) {
		return a * b;
	}

	@Override
	public Integer precedence() {		
		return 3;
	}

}
