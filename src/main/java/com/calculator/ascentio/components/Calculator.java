package com.calculator.ascentio.components;

import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.Map;
import java.util.Stack;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Calculator {

	@Resource
	private Map<Character, Operation> mapOperation;
	@Resource
	private Stack<Double> numbersStack;
	@Resource
	private Stack<Operation> operationStack;
	@Autowired
	private Add add;
	@Autowired
	private Subtraction subtraction;
	@Autowired
	private Multiply multiply;
	@Autowired
	private Divide divide;

	@PostConstruct
	private void init() {
		mapOperation.put('+', add);
		mapOperation.put('-', subtraction);
		mapOperation.put('*', multiply);
		mapOperation.put('/', divide);
	}

	public Double getNumber(CharacterIterator it) {
		String number = "";
		while (it.current() != CharacterIterator.DONE) {
			char ch = it.current();
			if (Character.isDigit(ch)) {
				number += ch;
			} else {
				break;
			}
			it.next();
		}
		return Double.parseDouble(number);
	}

	public String calculate(String expression) {
		String number = "";
		numbersStack.clear();
		operationStack.clear();
		CharacterIterator it = new StringCharacterIterator(expression);
		while (it.current() != CharacterIterator.DONE) {
			char ch = it.current();
			if (Character.isDigit(ch)) {
				number += ch;
			} else {
				buildStacks(Double.parseDouble(number), mapOperation.get(it.current()));
				number = "";
			}
			it.next();
		}
		numbersStack.push(Double.parseDouble(number));
		while (!operationStack.isEmpty()) {
			resolveOperation();
		}
		return numbersStack.pop().toString();
	}

	public void buildStacks(Double number, Operation operation) {
		if (numbersStack.isEmpty() || operationStack.isEmpty()
				|| (operation.precedence() > operationStack.peek().precedence())) {
			numbersStack.push(number);
			operationStack.push(operation);
		} else {
			numbersStack.push(number);
			resolveStacks(operation.precedence());
			operationStack.push(operation);
		}
	}

	public void resolveStacks(Integer precedence) {
		while (!operationStack.isEmpty() && operationStack.peek().precedence() >= precedence) {
			resolveOperation();
		}
	}

	public void resolveOperation() {
		Double second = numbersStack.pop();
		Double first = numbersStack.pop();
		Double result = operationStack.pop().calculate(first, second);
		numbersStack.push(result);
	}
}
