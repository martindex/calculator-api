package com.calculator.ascentio.services;

import java.util.List;

import com.calculator.ascentio.entities.Calculation;
import com.calculator.ascentio.entities.Session;

public interface CalculatorService {
	public String calculate(Calculation calculation);
	public void save(Session session, String ip);
	public List<Calculation> recover(String name, String ip);
}
