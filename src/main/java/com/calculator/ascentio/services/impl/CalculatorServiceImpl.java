package com.calculator.ascentio.services.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.calculator.ascentio.commons.Utils;
import com.calculator.ascentio.components.Calculator;
import com.calculator.ascentio.entities.Calculation;
import com.calculator.ascentio.entities.Session;
import com.calculator.ascentio.repositories.CalculationJpaRepository;
import com.calculator.ascentio.repositories.SessionJpaRepository;
import com.calculator.ascentio.services.CalculatorService;

@Service
public class CalculatorServiceImpl implements CalculatorService{

	@Autowired
	Calculator calculator;
	@Autowired
	CalculationJpaRepository calculationJpaRepository;
	@Autowired
	SessionJpaRepository sessionJpaRepository;

	@Override
	public String calculate(Calculation calculation) {
		String result = calculator.calculate(calculation.getExpression());
		calculation.setResult(result);
		calculation.setTime(Utils.getDateTimeNow());
		calculationJpaRepository.save(calculation);
		return result;
	}

	@Override
	public void save(Session session, String ip) {
		session = sessionJpaRepository.save(session);
		calculationJpaRepository.updateCalculationWhereIpEqualsAndIdSessionNull(session.getIdSession(), ip);
	}

	@Override
	public List<Calculation> recover(String name, String ip) {
		Session session = sessionJpaRepository.findByName(name);
		return calculationJpaRepository.findByIdSession(session.getIdSession());
	}

}
