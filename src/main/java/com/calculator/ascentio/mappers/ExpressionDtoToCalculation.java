package com.calculator.ascentio.mappers;

import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import com.calculator.ascentio.dtos.ExpressionDto;
import com.calculator.ascentio.entities.Calculation;

@Component
public class ExpressionDtoToCalculation extends PropertyMap<ExpressionDto, Calculation> {

	@Override
	protected void configure() {
		map().setExpression(source.getExpression());
	}

}
