package com.calculator.ascentio.mappers;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ConverterMapper extends org.modelmapper.ModelMapper{
	@Autowired
	CalculationToExpressionDto calculationToExpressionDto;
	@Autowired
	ExpressionDtoToCalculation expressionDtoToCalculation;
	@Autowired
	SessionDtoToSession sessionDtoToSession;
	@PostConstruct
	public void addMappings() {
		this.addMappings(calculationToExpressionDto);
		this.addMappings(expressionDtoToCalculation);
		this.addMappings(sessionDtoToSession);
	}
}
