package com.calculator.ascentio.mappers;

import org.modelmapper.PropertyMap;
import org.springframework.stereotype.Component;

import com.calculator.ascentio.dtos.SessionDto;
import com.calculator.ascentio.entities.Session;

@Component
public class SessionDtoToSession extends PropertyMap<SessionDto, Session> {

	@Override
	protected void configure() {
		map().setName(source.getSession());		
	}

}
