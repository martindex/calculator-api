package com.calculator.ascentio.repositories;

import java.io.Serializable;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.calculator.ascentio.entities.Calculation;

@Repository
public interface CalculationJpaRepository extends JpaRepository<Calculation, Serializable> {
	@Transactional
	@Modifying(clearAutomatically = true)
	@Query(value = "update calculation set id_session = :id where ip = :ip and id_session is null", nativeQuery = true)
	public int updateCalculationWhereIpEqualsAndIdSessionNull(@Param("id") Long id, @Param("ip") String ip);

	@Query(value = "select * from calculation where id_session = :id ", nativeQuery = true)
	public List<Calculation> findByIdSession(Long id);
}
