package com.calculator.ascentio.repositories;

import java.io.Serializable;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.calculator.ascentio.entities.Session;

@Repository
public interface SessionJpaRepository extends JpaRepository<Session, Serializable> {
	public Session findByName(String name);
}
