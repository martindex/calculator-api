package com.calculator.ascentio.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name = "calculation", catalog = "calculator")
public class Calculation {

	private Long idCalculation;
	private String ip;
	private String expression;
	private String result;
	private Date time;
	private Long idSession;

	public Calculation() {
	}

	public Calculation(Long idCalculation, String ip, String expression, String result, Date time, Long idSession) {
		this.idCalculation = idCalculation;
		this.ip = ip;
		this.expression = expression;
		this.result = result;
		this.time = time;
		this.idSession = idSession;
	}

	@Id
	@Column(name = "id_calculation")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_calculation_sequence")
	@SequenceGenerator(name = "id_calculation_sequence", sequenceName = "id_calculation_seq")
	public Long getIdCalculation() {
		return idCalculation;
	}

	public void setIdCalculation(Long idCalculation) {
		this.idCalculation = idCalculation;
	}

	@Column(name = "ip", nullable = false, length = 100)
	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

	@Column(name = "expression", nullable = false, length = 100)
	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	@Column(name = "result", nullable = false, length = 100)
	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "time", length = 19)
	public Date getTime() {
		return time;
	}

	public void setTime(Date time) {
		this.time = time;
	}

	@Column(name = "id_session")
	public Long getIdSession() {
		return idSession;
	}

	public void setIdSession(Long idSession) {
		this.idSession = idSession;
	}

}