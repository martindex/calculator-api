package com.calculator.ascentio.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "session", catalog = "calculator")
public class Session {

	private Long idSession;
	private String name;

	public Session() {
	}

	public Session(Long idSession, String name) {
		this.setIdSession(idSession);
		this.name = name;
	}

	@Id
	@Column(name = "id_session")
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "id_session_sequence")
	@SequenceGenerator(name = "id_session_sequence", sequenceName = "id_session_seq")
	public Long getIdSession() {
		return idSession;
	}

	public void setIdSession(Long idSession) {
		this.idSession = idSession;
	}

	@Column(name = "name")
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
