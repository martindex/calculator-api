package com.calculator.ascentio.controllers;

import java.lang.reflect.Type;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.calculator.ascentio.dtos.ExpressionDto;
import com.calculator.ascentio.dtos.SessionDto;
import com.calculator.ascentio.entities.Calculation;
import com.calculator.ascentio.entities.Session;
import com.calculator.ascentio.mappers.ConverterMapper;
import com.calculator.ascentio.services.CalculatorService;

@RestController
@RequestMapping("/calculator-api")
public class CalculatorController {

	@Autowired
	CalculatorService calculatorService;
	@Autowired
	private HttpServletRequest request;
	@Autowired
	ConverterMapper converterMapper;
	@Resource
	Type typeExpression;

	@PostMapping("/calculate")
	@ResponseStatus(HttpStatus.OK)
	public @ResponseBody ExpressionDto calculate(@Valid @RequestBody ExpressionDto expressionDto) {
		Calculation calculation = converterMapper.map(expressionDto, Calculation.class);
		calculation.setIp(request.getRemoteAddr());
		expressionDto.setResult(calculatorService.calculate(calculation));
		return expressionDto;
	}

	@PostMapping("/save")
	@ResponseStatus(value = HttpStatus.OK, reason = "saved session")
	public void saveSession(@Valid @RequestBody SessionDto sessionDto) {
		calculatorService.save(converterMapper.map(sessionDto, Session.class), request.getRemoteAddr());
	}

	@GetMapping("/recover")
	@ResponseStatus(HttpStatus.OK)
	public List<ExpressionDto> getCalculatesBySession(@Valid @RequestBody SessionDto sessionDto) {
		return converterMapper.map(calculatorService.recover(sessionDto.getSession(), request.getRemoteAddr()),
				typeExpression);
	}

}
