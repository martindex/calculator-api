package com.calculator.ascentio.commons;

import java.lang.reflect.Type;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.modelmapper.TypeToken;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.calculator.ascentio.components.Operation;
import com.calculator.ascentio.dtos.ExpressionDto;

@Configuration
public class ProjectConfiguration {

	@Bean("mapOperation")
	public Map<Character, Operation> mapOperation() {
		return new HashMap<Character, Operation>();
	}

	@Bean("numbersStack")
	public Stack<Double> numbersStack() {
		return new Stack<Double>();
	}

	@Bean("operationStack")
	public Stack<Operation> operationStack() {
		return new Stack<Operation>();
	}

	@Bean("typeExpression")
	public Type typeExpression() {
		return new TypeToken<List<ExpressionDto>>() {
		}.getType();
	}
}
