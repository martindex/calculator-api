package com.calculator.ascentio.commons;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

public class Utils {

	public static Date getDateTimeNow() {
		return Date.from(LocalDateTime.now().atZone(ZoneId.systemDefault()).toInstant());
	}

}
