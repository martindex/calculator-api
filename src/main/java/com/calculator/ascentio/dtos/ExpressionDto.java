package com.calculator.ascentio.dtos;

import javax.validation.constraints.NotNull;

public class ExpressionDto {

	@NotNull(message = "Expression required")
	private String expression;
	private String result;

	public String getExpression() {
		return expression;
	}

	public void setExpression(String expression) {
		this.expression = expression;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}
}
