package com.calculator.ascentio.dtos;

import javax.validation.constraints.NotNull;

public class SessionDto {
	
	@NotNull(message = "Name session required")
	private String session;

	public SessionDto() {		
	}

	public SessionDto(String session) {
		super();
		this.session = session;
	}

	public String getSession() {
		return session;
	}

	public void setSession(String session) {
		this.session = session;
	}
}
